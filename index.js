const fs = require('fs');
// Yeah RXJS !!! Because it's fun !
const { interval } = require('rxjs');
const { take } = require('rxjs/operators');

// We configure the delay before sending a new event.
const DELAY_BETWEEN_EVENTS = 1000;

// We read the list of events in the provided stream.json file that contains an opta feed.
const optaData = fs.readFileSync('stream.json');
const optaStream = JSON.parse(optaData);

// We are filtering the events using the ids we use to manage for our players.
const events = optaStream.Games.Game.Event.filter((e) =>
  [
    '1',
    '21',
    '2',
    '11',
    '14',
    '23',
    '16',
    '17',
    '33',
    '63',
    '38',
    '35',
    '3',
    '19',
    '20',
    '18',
    '36',
    '15',
    '30',
    '32',
    '43',
  ].some((id) => id === e['@attributes'].type_id),
);
console.log('NB opta events: ', events.length);

const outEvents = [];
// The following part is the base of an opta file :
// Event: is the list of events for the game.
// @attributes: meta for the game (id, teams, etc...)
// NB : if you want to change the opta feed in stream.json by an other feed, you will have to
// replace the 2 '@attributes' parts by the one from the new feed.
const baseOut = {
  Games: {
    Game: {
      Event: outEvents,
      '@attributes': {
        id: '1031297',
        away_score: '2',
        away_team_id: '15002',
        away_team_name: 'Maladrerie OS',
        competition_id: '34',
        competition_name: 'Friendly',
        game_date: '2018-10-13T17:00:00',
        home_score: '3',
        home_team_id: '13453',
        home_team_name: 'AG Caennaise',
        matchday: '2',
        period_1_start: '2018-10-13T17:04:15',
        period_2_start: '2018-10-13T18:08:13',
        season_id: '2018',
        season_name: 'Season 2018/2019',
      },
    },
    '@attributes': {
      timestamp: '2018-10-13T19:25:31',
    },
  },
};

const resOut = baseOut;

// Refer to the RxJS documentation for explanations on how to send data in an interval.
// Every DELAY_BETWEEN_EVENTS we take the next event from the filtered list we built just before
// and we send it to the outEvents array we declared previously.
// Then we replace Event in resOut.Games.Game and we overwrite the out.json file.
const interval$ = interval(DELAY_BETWEEN_EVENTS);
const delayedStream = interval$.pipe(take(events.length));
delayedStream.subscribe((i) => {
  console.log(events[i]);
  outEvents.push(events[i]);
  resOut.Games.Game.Event = outEvents;
  const outOpta = JSON.stringify(resOut);
  fs.writeFileSync('out.json', outOpta);
});
